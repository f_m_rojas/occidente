import { Component, OnInit } from '@angular/core';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import { MainbancService } from '../mainbanc.service';
import { Banc } from 'src/app/model/mainbanc';

@Component({
  selector: 'app-bussinessdata',
  templateUrl: './bussinessdata.component.html',
  styleUrls: ['./bussinessdata.component.styl']
})
export class BussinessdataComponent implements OnInit {
  dataBanc: Banc;
  faArrowLeft = faArrowLeft;
  constructor(private mainbancService: MainbancService) { }

  ngOnInit(): void {
    this.mainbancService.query({
      nit: "800220154"
    })
    .subscribe((res: any) => {
      this.dataBanc = res;
    }, error => {
      console.error('error ', error);
    });

  }
  }
