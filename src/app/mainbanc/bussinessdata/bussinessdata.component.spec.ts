import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BussinessdataComponent } from './bussinessdata.component';

describe('BussinessdataComponent', () => {
  let component: BussinessdataComponent;
  let fixture: ComponentFixture<BussinessdataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BussinessdataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BussinessdataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
