import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditlineComponent } from './creditline.component';

describe('CreditlineComponent', () => {
  let component: CreditlineComponent;
  let fixture: ComponentFixture<CreditlineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditlineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditlineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
