import { Component, OnInit } from '@angular/core';
import {faHandHoldingUsd} from '@fortawesome/free-solid-svg-icons';
import {faHome} from '@fortawesome/free-solid-svg-icons';
import {faExclamationCircle} from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-creditline',
  templateUrl: './creditline.component.html',
  styleUrls: ['./creditline.component.styl']
})
export class CreditlineComponent implements OnInit {
  faHandHoldingUsd = faHandHoldingUsd;
  faHome = faHome;
  faExclamationCircle = faExclamationCircle;
  operation = "BorderCreditLine";
  indicator = "listCreditLine";
  client = "listCreditLine";
  operationLogic = 0;
  constructor() { }

  ngOnInit(): void {
  }
  borderChangeOperation():void{
    this.client = "listCreditLine";
    this.indicator = "listCreditLine";
    this.operation = "BorderCreditLine";
  }
  borderChangeIndicator():void{
    this.client = "listCreditLine";
    this.indicator = "BorderCreditLine";
    this.operation = "listCreditLine";
  }
  borderChangeClient():void{
    this.client = "BorderCreditLine";
    this.indicator = "listCreditLine";
    this.operation = "listCreditLine";
  }
}
