import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterbancComponent } from './footerbanc.component';

describe('FooterbancComponent', () => {
  let component: FooterbancComponent;
  let fixture: ComponentFixture<FooterbancComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterbancComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterbancComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
