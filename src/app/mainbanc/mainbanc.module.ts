import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarbancComponent } from './navbarbanc/navbarbanc.component';
import { BussinessdataComponent } from './bussinessdata/bussinessdata.component';
import { CreditlineComponent } from './creditline/creditline.component';
import { FooterbancComponent } from './footerbanc/footerbanc.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [NavbarbancComponent, BussinessdataComponent, CreditlineComponent, FooterbancComponent],
  imports: [
    CommonModule,
    FontAwesomeModule,
    HttpClientModule
  ],
  exports:[
    NavbarbancComponent,
    BussinessdataComponent,
    CreditlineComponent,
    FooterbancComponent
  ]
})
export class MainbancModule { }
