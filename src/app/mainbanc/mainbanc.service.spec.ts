import { TestBed } from '@angular/core/testing';

import { MainbancService } from './mainbanc.service';

describe('MainbancService', () => {
  let service: MainbancService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MainbancService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
