import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Banc } from '../model/mainbanc';
import { createRequestOption } from 'src/app/solicitud/utilidad_solicitud';


@Injectable({
  providedIn: 'root'
})
export class MainbancService {

  constructor(private http: HttpClient) {}
  public query(req?: any): Observable<Banc> {
    const params = createRequestOption(req);
    return this.http.get<Banc>(`${environment.END_POINT}/apiPruebaFmr`, { params: params })
    .pipe(map(res => {
      return res;
    }));
  }

}
