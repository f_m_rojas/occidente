import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarbancComponent } from './navbarbanc.component';

describe('NavbarbancComponent', () => {
  let component: NavbarbancComponent;
  let fixture: ComponentFixture<NavbarbancComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavbarbancComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarbancComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
