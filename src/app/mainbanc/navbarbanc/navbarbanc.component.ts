import { Component, OnInit } from '@angular/core';
import {faArrowLeft} from '@fortawesome/free-solid-svg-icons';
import { MainbancService } from '../mainbanc.service';
import { Banc } from 'src/app/model/mainbanc';

@Component({
  selector: 'app-navbarbanc',
  templateUrl: './navbarbanc.component.html',
  styleUrls: ['./navbarbanc.component.styl']
})
export class NavbarbancComponent implements OnInit {
  faArrowLeft = faArrowLeft;
  infoBanco: Banc;
  constructor(private mainbancService:MainbancService) { }

  ngOnInit(): void {
    this.mainbancService.query({
      nit: "800220154"
    })
    .subscribe((res: any) => {
      this.infoBanco = res;
    }, error => {
      console.error('error ', error);
    });

  }
}
