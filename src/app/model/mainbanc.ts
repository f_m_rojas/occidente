export interface Banc {
    nit?: string;
    nombre: string;
    capa: string;
    segmento: string;
    gerenteRelacion: string;
}
